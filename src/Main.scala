import java.{ util => ju }
import scala.annotation._
import scala.collection._
import scala.collection.{ mutable => mu }
import scala.collection.JavaConverters._
import scala.math._
import scala.io.Source
import java.io.PrintWriter
import java.io.File
import scala.util.control.Breaks

object Main extends App {
  val reader = Source.fromFile("input.txt").getLines;
  val writer = new PrintWriter(new File("output.txt" ));
  val N = reader.next().toInt;
  (0 to (N-1)).foreach(i =>{
    var k = reader.next().toInt
    writer.println(reader.next().split(" ").groupBy(_.charAt(0)).map(_._2.map(_.toInt).sum).max)
  })
  writer.close
}