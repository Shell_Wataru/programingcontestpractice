object two extends App {
  import java.{ util => ju }
  import scala.annotation._
  import scala.collection._
  import scala.collection.{ mutable => mu }
  import scala.collection.JavaConverters._
  import scala.math._
  import scala.io.Source
  import java.io.PrintWriter
  import java.io.File
  import scala.util.control.Breaks

  val reader = Source.fromFile("input.txt").getLines;
  val writer = new PrintWriter(new File("output.txt"));
  val N = reader.next().toInt;
  (0 to (N - 1)).foreach(i => {
    var s = reader.next().split(" ")
    var length = s.map(_.length).sum * 1.0 / s.length
    var k = reader.next().toInt
    var texts: Array[String] = Array()
    var hannin = ""
    var min = 1000000000.0
    (0 to (k - 1)).foreach(j => {
      var t = reader.next().split(" ")
      var t_length = t.tail.map(_.length).sum * 1.0/ t.tail.length 
      var gosa = math.abs(t_length - length)
      if (gosa < min) {
        min = gosa
        hannin = t(0).init
      }
    })
    writer.println(hannin)
  })
  writer.close
}