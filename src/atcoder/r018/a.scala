package atcoder.r018

object a extends App {
  var data = readLine.split(" ").map(_.toDouble)
  var Height = data(0) * 0.01
  var BMI = data(1)
  println( Height * Height * BMI)
}