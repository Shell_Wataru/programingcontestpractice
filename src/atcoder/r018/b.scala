package atcoder.r018

object b {
  def main(arg: Array[String]) {
    var N = readInt
    var dots: Array[Array[Long]] = Array()
    (0 to N - 1) foreach { i =>
      dots = dots :+ readLine.split(" ").map(_.toLong)
    }
    var count = 0
    for (i <- 0 to N - 3; j <- i + 1 to N - 2; k <- j + 1 to N - 1) {
      if (check(dots(i), dots(j), dots(k)))
        count += 1
    }
    println(count)
  }
  def check(a: Array[Long], b: Array[Long], c: Array[Long]): Boolean = {
    var x0 = a(0) - b(0)
    var y0 = a(1) - b(1)
    var x1 = a(0) - c(0)
    var y1 = a(1) - c(1)
    var s = math.abs(x0 * y1 - x1 * y0)
    s % 2 == 0 && s != 0
  }
}