package atcoder.r018

object d {
  def main(arg: Array[String]){
    val data1 = readLine.split(" ").map(_.toInt)
    val N = data1(0)
    val M = data1(1)
    var cluster = Array.range(0,N)
    var total_cost = 0
    var max_edge_cost = 0
    var Edges = Array.fill(M)(readLine.split(" ").map(_.toInt)).sortWith(_(2) < _(2))
    var Used_Edges:Array[Array[Int]] = Array()
    Edges.foreach { edge =>
      if(cluster(edge(0) - 1) != cluster(edge(1) - 1)){
        var min_cluster = math.min(cluster(edge(0) -  1),cluster(edge(1) - 1)) 
        cluster(edge(0)-1) = min_cluster
        cluster(edge(1)-1) = min_cluster
        total_cost += edge(2)
        max_edge_cost = edge(2)
        Used_Edges :+= edge
      }
    }
    var cluster2 = Array.range(0,N)
    Used_Edges.filter(_(3) < max_edge_cost).foreach { edge =>
      if(cluster(edge(0) - 1) != cluster2(edge(1) - 1)){
        var min_cluster = math.min(cluster2(edge(0) -  1),cluster2(edge(1) - 1)) 
        cluster2(edge(0)-1) = min_cluster
        cluster2(edge(1)-1) = min_cluster
      }
    }
    println(total_cost + " " + 1)
  }
}