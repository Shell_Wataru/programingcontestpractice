package atcoder.r016

import scala.collection.BitSet

object arc016c extends App{
  var data = readLine split ' ' map(_.toInt)
  var N = data (0)
  var M = data(1)
  var map:Map[String,Double] = Map()
  var toto:Array[Array[Array[Int]]] = Array()
  var costs = new Array[Int](M)
  for(i <-0 to M -1){
    var temp =readLine split ' ' map(_.toInt)
    costs(i) = temp(1)
    toto :+= Array.fill[Array[Int]](temp(0))(readLine split ' ' map(_.toInt))
  }
  println(toto.deep)
  println(one_cost(BitSet(),1))
  def needs_cost(status:BitSet):Double ={
    if (map.contains(status.toString))
      return map(status.toString)
    else{
      var value = status.map(i => needs_cost(status - i)).sum
      map += status.toString -> value 
      map(status.toString)
    }
  }
  def one_cost(before_status:BitSet,get_status:Int):Double = {
    var totocost =(toto zip costs).minBy(totocost =>totocost._2 * 100 / totocost._1.filter(detail => !before_status(detail(0))).map(_(1)).sum)
    totocost._2 * 100 / totocost._1.filter(detail => detail(0) == get_status).map(_(1)).sum
  }
}