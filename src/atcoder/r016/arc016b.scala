package atcoder.r016

object arc016b {
  def main(arg: Array[String]) {
    var N = readInt
    var status = new Array[Int](9)
    var count = 0
    (0 to N - 1).foreach { i =>
      var line = readLine
      (0 to 8).foreach {j =>
        line(j) match {
          case '.' =>
            status(j) = 0
          case 'x' =>
            status(j) = 0
            count += 1
          case 'o' =>
            if (status(j) == 0){
              status(j) = 1
              count += 1
            }
        }
      }
    }
    println(count)
  }
}