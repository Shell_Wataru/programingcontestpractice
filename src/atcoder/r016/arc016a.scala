package atcoder.r016

object arc016a {
  def main(arg:Array[String]){
    var data = readLine split " " map(_.toInt)
    var N = data(0)
    var M = data(1)
    println(if (M== 1) 2 else 1) 
  }
}