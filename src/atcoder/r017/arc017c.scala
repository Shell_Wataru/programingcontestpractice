package atcoder.r017

object arc017c {
  var map: Map[String, Int] = Map()
  def main(arg: Array[String]) {
    var data = readLine.split(" ").map(_.toInt)
    var N = data(0)
    var X = data(1)
    var weights = Array.fill(N)(readInt).groupBy(_.toInt).map(weight => (weight._1, weight._2.length)).toArray
    println(check(X,weights,0))
  }

  def check(X: Int, weights: Array[Tuple2[Int, Int]], i: Int): Int = {
    if (X <= 0 || i == weights.length)
      0
    else if (map.contains(situ(X, i))) {
      map(situ(X, i))
    } else if (X % weights(i)._1 == 0 && X / weights(i)._1 <= weights(i)._2) {
      var temp = (situ(X, i) -> (Array.range(0, weights(i)._2 + 1).map(index => choose(weights(i)._2,index) * check(X - index * weights(i)._1, weights, i + 1)).sum + choose(weights(i)._2,X / weights(i)._1)))
      map = map + temp 
      map(situ(X, i))
    } else {
      var temp = (situ(X, i) -> Array.range(0, weights(i)._2 + 1).map(index => choose(weights(i)._2,index) * check(X - index * weights(i)._1, weights, i + 1)).sum)
      map = map + temp
      map(situ(X, i))
    }
  }
  def situ(X:Int,i:Int) :String ={
    X + "-" + i
  }
  def choose(n: Int, k: Int):Int = {
    var list = (n - k + 1 to n) zip (1 to k.toInt).reverse
    list.foldRight(1)((a, b) => { ((a._1 * b / a._2)) })
  }
}