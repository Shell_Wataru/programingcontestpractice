package atcoder.r017

object arc017a {
  def main(arg: Array[String]) {
   var N = readInt
   var numbers = Array(2 , math.sqrt(N).floor.toInt + 1)
   var flag = true
   (2 to math.sqrt(N).floor.toInt).foreach { i =>
     if(N % i == 0)
       flag =false
   }
   if(flag)
     println("YES")
   else
     println("NO")
  }
}