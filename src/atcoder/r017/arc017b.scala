package atcoder.r017

object arc017b {
  def main(arg: Array[String]) {
    var data = readLine.split(" ").map(_.toInt)
    var N = data(0)
    var K = data(1)
    var before_score = -1
    var now_score = 0
    var continue = 0
    var count = 0
    (0 to N - 1).foreach { i =>
      now_score = readInt
      if (now_score > before_score) {
        continue = math.min(K, continue + 1)
        if (continue == K)
          count += 1
      } else {
        continue = 1
        if (continue == K)
          count += 1
      }
      before_score = now_score
    }
    println(count)
  }
}