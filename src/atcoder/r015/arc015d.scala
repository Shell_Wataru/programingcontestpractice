package atcoder.r015

object arc015d {
  def main(args: Array[String]) {
    var data = readLine split ' ' map (_.toDouble)
    var T = data(0).toInt
    var N = data(1).toInt
    var P = data(2)
    var data_array = Array.fill(N)(readLine split ' ' map (_.toDouble)).sortWith(_(2) < _(2))
    var temp_array = data_array.clone
    var max_t = data_array.map(_(2)).max.toInt
    var rate_array = Array((0,1.0))
    var before_t = 0
    while(temp_array.length != 0){
        var t = temp_array.head(2).toInt
        var rate = temp_array.map(array => array(0) * array(1)).sum * P + (1 - temp_array.map(array => array(0) ).sum * P ) * 1
        rate_array :+= (t,rate)
        before_t = t
        temp_array = temp_array.filter(t < _(2))
    }
    var count = BigDecimal(0.0)
    var rate = BigDecimal(1.0)
    println(rate_array.deep)
    (1 to rate_array.length  - 1).foreach{ i =>
    }
    println(count)
  }
}