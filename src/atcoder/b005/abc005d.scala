package atcoder.b005

object abc005d extends App {
  var N = readInt
  var D = Array.fill(N)(readLine split ' ' map (_.toInt))
  var Q = readInt
  var S = Array.ofDim[Int](N + 1, N + 1)
  var calculated :Map[Int,Int] = Map() 
  for (i <- 1 to N; j <- 1 to N) {
    S(i)(j) = S(i - 1)(j) + S(i)(j - 1) - S(i - 1)(j - 1) + D(i - 1)(j - 1)
  }
  (0 to Q - 1).foreach { q =>
    println(dmax(N, S, readInt))
  }
  def dmax(N: Int, D: Array[Array[Int]], P: Int): Int = {
    var max_v = 0
    (1 to math.sqrt(P).toInt).foreach { n =>
      var y = math.min(P / n, N)
      var x = n
      if (y != 0 && math.min(P / n, N) != math.min(P / (n + 1), N)) {
        max_v = Array(max_value(x, y, S), max_value(y, x, S), max_v).max
      }
    }
    max_v
  }
  def max_value(x: Int, y: Int, S: Array[Array[Int]]): Int = {
    var max_v = 0
    for (i <- 0 to N - x; j <- 0 to N - y) {
      var v = value(i,j,x,y,S)
      if (v > max_v)
        max_v = v
    }
    max_v
  }
  def value(i: Int, j: Int, x: Int, y: Int, S: Array[Array[Int]]): Int = {
    S(i + x)(j + y) - S(i)(j + y) - S(i + x)(j) + S(i)(j)
  }
}