package atcoder.b005

object abc005a extends App{
  val data = readLine split ' ' map(_.toInt)
  println(data(1)/data(0))
}