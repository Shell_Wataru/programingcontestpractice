package atcoder.b005

object abc005b extends App {
  val N = readInt
  var return_t = 1000
  (0 to N - 1).foreach { i =>
    val t = readInt
    if (t < return_t)
      return_t = t
  }
  println(return_t)
}