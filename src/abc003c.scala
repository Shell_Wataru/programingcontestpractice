object abc003c extends App {
  var First = readLine.split(" ").map(_.toInt)
  var N = First(0)
  var K = First(1)
  println(
    readLine.split(" ").map(_.toInt)
      .sortWith(_ > _).take(K)
      .foldRight(0: Double)((a, b) => {(a + b) / 2 }))
}