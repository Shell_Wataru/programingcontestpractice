object abc003d extends App {
  val first = readLine.split(" ").map(_.toInt)
  val second = readLine.split(" ").map(_.toInt)
  val third = readLine.split(" ").map(_.toInt)

  var R = first(0)
  var C = first(1)
  var X = second(0)
  var Y = second(1)
  var D = third(0)
  var L = third(1)
  val base = BigInt(math.pow(10, 9).toInt + 7)
  var map = Array.fill(X + 1)(Array.fill(Y + 1)(BigInt(0)))

  def choose(n: BigInt, k: BigInt) = {
    var list = (n - k + 1 to n) zip (1 to k.toInt).reverse
    list.foldRight(BigInt(1))((a, b) => { ((a._1 * b / a._2)) })
  }
  def choose3(i: BigInt, j: BigInt, k: BigInt): BigInt = {
    choose(i + j + k, i) * choose(j + k, k)
  }

  def f(R: Int, C: Int, X: Int, Y: Int, D: Int, L: Int): BigInt = {
    if (map(X)(Y) == 0)
      map(X)(Y) = (choose3(X * Y - D - L, D, L) - sigma(X, Y, D, L)) % base
    (R - X + 1) * (C - Y + 1) * map(X)(Y)
  }
  def sigma(X: Int, Y: Int, D: Int, L: Int): BigInt = {
    var res = BigInt(0)
    (1 to X).foreach { x =>
      (1 to Y).foreach { y =>
        if (x * y != X * Y && x * y >= D + L) {
          res += f(X, Y, x, y, D, L)
        }
      }
    }
    res
  }
  println(f(R, C, X, Y, D, L) % base)
}