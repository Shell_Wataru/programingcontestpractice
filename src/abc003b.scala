object abc003b extends App {
  var S = readLine
  var T = readLine
  var flag = true
  (0 to S.length - 1).foreach(i => {
    if (S(i) != '@' && T(i) != '@' && S(i) != T(i)) {
     flag = false
    }else if(S(i) == '@' && !"@atcoder".contains(T(i)) ){
      flag = false
    }else if(T(i) == '@' && !"@atcoder".contains(S(i))){
      flag = false
    }
  })
  if(flag){
    println("You can win")
  }else{
    println("You will lose")
  }
}