object three extends App {
  import java.{ util => ju }
  import scala.annotation._
  import scala.collection._
  import scala.collection.{ mutable => mu }
  import scala.collection.JavaConverters._
  import scala.math._
  import scala.io.Source
  import java.io.PrintWriter
  import java.io.File
  import scala.util.control.Breaks

  val reader = Source.fromFile("input.txt").getLines;
  val writer = new PrintWriter(new File("output.txt"));
  val N = reader.next().toInt;
  (0 to (N - 1)).foreach(i => {
    var k = reader.next().toInt;
    var cards = reader.next()
    println(cards)
    var j = 0
    var players = Array.fill(k)(0L)
    var turn = 0
    while (j < cards.length) {
      var card = cards.charAt(j).toString
      println(j)
      card match {
        case "X" =>
          var continue = true
          if (j == cards.length -1)
            j += 1
          while (continue && j < cards.length - 1) {
            j += 1
            var next_card = cards.charAt(j).toString
            if (next_card != "X" & next_card != "D" & next_card != "S") {
              players(turn) = players(turn) * next_card.toInt
              continue = false
              j += 1
            }
          }
        case "D" =>
          var continue = true
          if (j == cards.length -1)
            j += 1
          while (continue && j < cards.length - 1) {
            j += 1
            var next_card = cards.charAt(j).toString
            if (next_card != "X" & next_card != "D" & next_card != "S") {
              players(turn) = players(turn) / next_card.toInt
              continue = false
              j += 1
            }
          }
        case "S" =>
          var continue = true
          if (j == cards.length -1)
            j += 1
          while (continue && j < cards.length - 1) {
            j += 1
            var next_card = cards.charAt(j).toString
            if (next_card != "X" & next_card != "D" & next_card != "S") {
              players(turn) -= next_card.toInt
              continue = false
              j += 1
            }
          }
        case _ =>
          players(turn) += card.toInt
          j += 1
      }
      turn = (turn + 1) % k
    }
    writer.println(players.max)
  })
  writer.close
}